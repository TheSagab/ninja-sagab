# Sistem Cerdas Kelas C - Kelompok Ninja Sagab

# Judul Proyek
Hate Speech Detector

## Anggota
1. Anindito Bhagawanta - 1606879230
2. Ibnu Sofian Firdaus - 1606893986
3. Mohamad Mahendra - 1606879224

# Citations and Credits :
Ika Alfina, Rio Mulia, Mohamad Ivan Fanany, and Yudo Ekanata, "Hate Speech Detection in Indonesian Language: A Dataset and Preliminary Study ", in Proceeding of 9th International Conference on Advanced Computer Science and Information Systems 2017. ICACSIS) 2017.
(https://www.researchgate.net/publication/320131169_Hate_Speech_Detection_in_the_Indonesian_Language_A_Dataset_and_Preliminary_Study)
