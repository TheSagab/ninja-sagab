from lib.hate_speech_filter import classify

def main():
    while True:
        masukan = input("Masukkan pesan : ")

        if(masukan=="exit"):
            break
        print(classify(masukan))


# file ini kalo dijalanin langsung akan menjalankan fungsi main
if __name__ == "__main__":
    # Buat testing
    print("Testing : ")
    print(classify("bodoh munafik bego")) # nilai harusnya 1, bobot HS mutlak 1
    print(classify("sok yg pilih orang")) # nilai harusnya 1, tapi tidak mutlak 1 bobot HS nya
    print(classify("pilkada dgn ga habis")) # nilai harusnya 2
    print(classify("mata lu bego")) # nilai harusnya 3
    print(classify("halo apakareba skips skips")) # nilai harusnya 3
    print("===============================")
    main()