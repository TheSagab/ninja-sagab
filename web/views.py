from django.shortcuts import render
from django.http import JsonResponse
from lib.hate_speech_filter import classify
from .forms import Input_Form
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

def index(request):
    return render(request, 'halaman.html', {'form': Input_Form})


@csrf_exempt
def predict_word(request):    
    form = Input_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        str_input = request.POST['input']
        category, hs_probability, non_hs_probability = classify(str_input)
        if category == 1:
        	category = "Hate Speech"
        elif category == 2:
        	category = "Non Hate Speech"
        else:
        	category = "Undecided"
        	hs_probability = 0
        	non_hs_probability = 0
        return JsonResponse({
            'category': category,
            'hs_probability': hs_probability*100,
            'non_hs_probability': non_hs_probability*100
            })
