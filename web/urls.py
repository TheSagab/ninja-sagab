from django.urls import path
from .views import index, predict_word

app_name = "web";

urlpatterns = [
    path(r'', index, name='index'),
    path(r'predict', predict_word, name='predict_word')
]
