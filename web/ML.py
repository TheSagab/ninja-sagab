from collections import Counter
import re
import pandas

headlines = [
    "PretzelBros, airbnb for people who like pretzels, raises $2 million",
    "Top 10 reasons why Go is better than whatever language you use.",
    "Why working at apple stole my soul (I still love it though)",
    "lagian aku lagi main digangguin masih",
]

file = open("stopwords.txt","r")
Stopwords = [re.sub(r'[^\w\s\d]','',h.lower()) for h in file.readlines()]

Stopwords = [re.sub("\s+", " ", h) for h in Stopwords]

listStopwords = list(set(" ".join(Stopwords).split(" ")))

# Lowercase, then replace any non-letter, space, or digit character in the headlines.
new_headlines = [re.sub(r'[^\w\s\d]','',h.lower()) for h in headlines]
# Replace sequences of whitespace with a space character.
new_headlines = [re.sub("\s+", " ", h) for h in new_headlines]

# Find all the unique words in the headlines.
unique_words = list(set(" ".join(new_headlines).split(" ")))

newword = []
for x in unique_words:
    if x not in listStopwords:
        newword.append(x)

print (newword)

def make_matrix(headlines, vocab):
    matrix = []
    for headline in headlines:
        # Count each word in the headline, and make a dictionary.
        counter = Counter(headline)
        # Turn the dictionary into a matrix row using the vocab.
        row = [counter.get(w, 0) for w in vocab]
        matrix.append(row)
    df = pandas.DataFrame(matrix)
    df.columns = newword
    return df

print(make_matrix(headlines, newword))
