from django import forms

class Input_Form(forms.Form):
    attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'form-control',
        'id': 'predict',
        'placeholder':'Masukkan pesan disini...'
    }

    input = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=attrs))
