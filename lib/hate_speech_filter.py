import math
import numpy as np
from lib.text_processor import clean_line, create_stopword_stopsign_acronym
from lib.naive_bayes import get_kb

def calculate_probability(word_list, kb):
    hs_prob = 1
    non_hs_prob = 1
    for word in word_list:
        try:
            prob = kb[word]
            hs_prob *= prob
            non_hs_prob *= (1-prob)
        except KeyError:
            continue
    return (hs_prob,non_hs_prob)

# Fungsi ini yang akan dipanggil oleh web
# Return :
#           1 - Hate Speech
#           2 - Non Hate Speech
#           3 - Undecided, Ada kata mutlak hate speech, dan kata mutlak non hate speech
#           4 - Undecided, Antara Netral atau semua kata-kata di message tidak ada di KB

def classify(message):
    kb = get_kb()
    stop_sign_list = []
    stop_word_list = []
    dict_acronym_word = {}
    create_stopword_stopsign_acronym(stop_word_list,stop_sign_list,dict_acronym_word)

    message = clean_line(message, stop_word_list, stop_sign_list, dict_acronym_word)
    probabilities = calculate_probability(message.split(" "), kb)
    hs_probability = probabilities[0]
    non_hs_probability = probabilities[1]

    category = 3
    if(hs_probability > non_hs_probability):
        category = 1
    elif(hs_probability < non_hs_probability):
        category = 2
    elif(hs_probability == 0 and non_hs_probability == 0):
        category = 3
    else:
        category = 4
    return((category, hs_probability, non_hs_probability))