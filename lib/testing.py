from lib.hate_speech_filter import classify
import os

path = os.path.dirname(os.path.abspath(__file__))

def runtest():
    
    #Menghitung dari test set HS
    true_positives = 0
    relevant_elements = 0

    file = open(path + "/testDataSetHS.txt","r")

    for line in file.readlines():
        result = classify(line)
        if(result[0] == 1):
            true_positives += 1
        relevant_elements += 1
    file.close()

    # Menghitung dari test set Non HS
    false_positives = 0
    irrelevant_elements = 0

    file = open(path + "/testDataSetNonHS.txt","r")

    for line in file.readlines():
        result = classify(line)
        if(result[0] == 1):
            false_positives += 1
        irrelevant_elements += 1
    file.close()

    precision = true_positives / (true_positives + false_positives)
    recall = true_positives / relevant_elements
    f_measure = 2 * ( (precision * recall) / (precision + recall) )

    print("From",relevant_elements,"hate speeches.",true_positives,"were marked positive hate speeches")
    print("From",irrelevant_elements,"non-hate speeches.",false_positives,"were marked positive hate speeches")
    print("Precission :",precision*100,"%")
    print("Recall :",recall*100,"%")

    print("F-measure score : ", f_measure*100,"%")


