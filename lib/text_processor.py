from collections import Counter
from string import punctuation
import os

path = os.path.dirname(os.path.abspath(__file__))

def create_stopword_stopsign_acronym(list_stopwords, list_stopsigns, dict_acronym):
    file = open(path + "/stopWords.txt","r")
    set_stopwords = set()
    
    for word in file.readlines():
        set_stopwords.add(word.lower().strip())
    list_stopwords.extend(list(set_stopwords))
    file.close()

    file = open(path + "/stopSigns.txt","r")
    set_stopsigns = set()

    for word in file.readlines():
        set_stopsigns.add(word.lower().strip())
    list_stopsigns.extend(list(set_stopsigns))
    file.close()

    file = open(path + "/acronymWords.txt","r")
    for data in file.readlines():
        data_split = data.split(",")
        dict_acronym[data_split[0]] = data_split[1]
    file.close()


def remove_stop_sign(line,stopsigns):
    for word in line.split():
        for sign in stopsigns:
            if sign ==  word[:len(sign)]:
                line = line.replace(word,"")
    return line

def remove_symbols(line,symb):
    for s in symb:
        line = line.replace(s," ")
    return line

def remove_stop_word(line, stopwords):
    for word in line.split(" "):
        if word in stopwords:
            line = line.replace(word," ")
    return line

def replace_acronym(line, acronyms):
    for word in line.split(" "):
        if word in acronyms.keys():
            line = line.replace(word,acronyms[word])
    return line

def stemline(line):
    front = ["meng","mem","men","ber","be","di","me","ke"]
    rear = ["nya","kan","an"]

    clean_words = []
    for word in line.split(" "):
        for f in front:
            if word[:len(f)] == f and len(word) > 6:
                word = word[len(f):]
        
        for r in rear:
            if word[-len(r):] == r and len(word) > 6:
                word = word[:-len(r)]
        clean_words.append(word)

    line = " ".join(clean_words)
    return line
    

def clean_line(line, list_stopwords, list_stopsigns, dict_acronym):
    
    line = line.lower().strip()
    line = remove_stop_sign(line,list_stopsigns)
    
    punc = punctuation + "1234567890"
    line = remove_symbols(line, punc)
    
    line = remove_stop_word(line,list_stopwords)
    line = replace_acronym(line,dict_acronym)
    line = stemline(line)
    
    return " ".join(line.split())

def create_bow(filename):
    file = open(path + filename,"r")
    dict_bow = {}
    counter = 0
    list_stopsigns = []
    list_stopwords = []
    dict_acronymwords = {}
    create_stopword_stopsign_acronym(list_stopwords, list_stopsigns, dict_acronymwords)

    for line in file.readlines():
        counter += 1
        line = clean_line(line,list_stopwords, list_stopsigns, dict_acronymwords)
        set_temp = set(line.split(" "))
        
        for word in set_temp:
            if word not in dict_bow:
                dict_bow[word] = 1
            else:
                dict_bow[word] += 1
    
    file.close()
    return [dict_bow,counter]