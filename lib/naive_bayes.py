from lib.text_processor import create_bow
import os

path = os.path.dirname(os.path.abspath(__file__))

def single_probability(dict, key, total):
    try:
        occurence = dict[key]
        ratio = occurence/total
        return ratio
    except KeyError:
        return 0

def learn():
    data_hs = create_bow("/trainingDataSetHS.txt")
    bow_hs = data_hs[0]
    count_hs = data_hs[1]

    data_non_hs = create_bow("/trainingDataSetNonHS.txt")
    bow_non_hs = data_non_hs[0]
    count_non_hs = data_non_hs[1]
    
    target_file = open(path + "/knowledge_base.txt","w")
    word_list = list(bow_hs.keys())
    word_list.extend(list(bow_non_hs.keys()))
    word_set = set(word_list)
    for key in word_set:
        try:
            if(bow_hs[key] >= 3):
                hs_occurence = single_probability(bow_hs, key, count_hs)
                non_hs_occurence = single_probability(bow_non_hs, key, count_non_hs)

                hs_probability = hs_occurence / (hs_occurence + non_hs_occurence)
                new_knowledge = key + "," + str(hs_probability)
                print(new_knowledge, file = target_file)
        except KeyError:
            if(bow_non_hs[key] >= 3):
                new_knowledge = key + "," + str(0)
                print(new_knowledge, file = target_file)
    target_file.close()

def get_kb():
    try:
        file_kb = open(path + "/knowledge_base.txt","r")
    except FileNotFoundError:
        learn()
        file_kb = open(path + "/knowledge_base.txt","r")
    
    kb = {}
    for line in file_kb.readlines():
        data = line.strip().split(",")
        key = data[0]
        prob = float(data[1])
        kb[key] = prob
    
    file_kb.close()
    return(kb)
